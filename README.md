# i3-rice-dot-files

Fully working beta versions of my i3 and i3block config files to achieve setup as shown in
screenshots directory.

Features:

    • Simple and non-intidimating elegant UI
    • Suitable for new i3wm users
    • Taskbar includes stats for CPU temp, CPU load, RAM load, network load, free space in home
      and root directories, date, time, clickable (left/right) volume control and systray

Dependencies:

    • i3-gaps                   (core package)
    • i3blocks                  (core package)
    • i3lock                    (core package)
    * i3lock-fancy-git          (core package, works together with i3lock for elegant screenlock)
    • rofi                      (application launcher, better replacement of dmenu)
    • compton                   (used for window effects)
    • feh                       (used for setting background)
    • scrot                     (used for screen capture using Print key)
    • sysstat                   (used for system stats)
    • awesome-terminal-fonts    (used for icons)
    • lxappearance              (optional to set themes)

AUR package:

    • i3lock-fancy-git          (screenlock, better replacement of i3lock)

Credits:
    • Wallpaper: Unknown author, I picked it up using Google image search.
    • Developers of i3wm, i3blocks and related packages.

Notes:
1. Package names are based on Arch Linux repositories, distro specific names may vary slightly.
2. Config files use ‘noto-fonts’ as system font which can be changed by users.

Important notes:
1. Place all files to ~/.config/i3/
2. User will have to make slight changes in package varables to add their chosen
   software. For example, firefox users will have to replace chromium.

This is my first attempt at ricing i3, therefore, I consider my dot files a 'work in progress'
and these files will evolve over time as I gain i3wm experience and end-up with more elegant UI.
